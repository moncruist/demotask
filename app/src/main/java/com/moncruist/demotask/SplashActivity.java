package com.moncruist.demotask;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.os.Bundle;

public class SplashActivity extends SingleFragmentActivity {
	private static final int DELAY_MILLIS = 2000;
	private static final String KEY_START_TIME = SplashActivity.class.getCanonicalName() + ".start_time";
	private static final int NOT_POSTED = -1;

	private Handler handler;
	private long startTime;
	private Runnable showHomeActivity = new Runnable() {
		@Override
		public void run() {
			startTime = NOT_POSTED;
			Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
			startActivity(intent);
			finish();
		}
	};

	@Override
    protected Fragment createFragment() {
        return new SplashFragment();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

		if (savedInstanceState != null) {
			startTime = savedInstanceState.getLong(KEY_START_TIME, NOT_POSTED);
		} else {
			startTime = NOT_POSTED;
		}

		handler = new Handler();
		if (startTime == NOT_POSTED) {
			// Post runnable for the first time
			startTime = System.currentTimeMillis();
			handler.postDelayed(showHomeActivity, DELAY_MILLIS);
		} else {
			// Activity was recreated before runnable was executed
			long currentTime = System.currentTimeMillis();
			long delay = DELAY_MILLIS  + startTime - currentTime;
			if (delay <= 0) {
				// Missed deadline. Just run runnable
				showHomeActivity.run();
			} else {
				handler.postDelayed(showHomeActivity, delay);
			}
		}
    }

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putLong(KEY_START_TIME, startTime);
	}

	@Override
	protected void onPause() {
		super.onPause();
		handler.removeCallbacks(showHomeActivity);
	}
}
