package com.moncruist.demotask;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.ref.WeakReference;
import java.util.List;


public class HomeFragment extends Fragment {

	private static final String TAG = "HomeFragment";
	private static final String KEY_CAT_FILE = HomeFragment.class.getName() + ".cat_file";
	private static final int LOADER_ID_CAT = 0;

	private ProgressBar downloadProgressBar;
	private Button button;
	private TextView statusLabel;

	private FragmentProgressHandler handler;
	private ImageDownloaderCallbacks loaderCallbacks;
	private String catFile;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (savedInstanceState != null) {
			catFile = savedInstanceState.getString(KEY_CAT_FILE, null);
		} else {
			catFile = null;
		}

		handler = new FragmentProgressHandler(this);
		loaderCallbacks = new ImageDownloaderCallbacks();
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.f_home, container, false);

		downloadProgressBar = (ProgressBar) view.findViewById(R.id.home_download_progress);
		button = (Button) view.findViewById(R.id.home_download_button);
		statusLabel = (TextView) view.findViewById(R.id.home_status_label);

		if (catFile == null) {
			downloadProgressBar.setVisibility(View.INVISIBLE);
			button.setOnClickListener(new DownloadClickListener());
			setIdleMessage();
		} else {
			button.setOnClickListener(new OpenClickListener());
			button.setText(R.string.home_button_text_open);
			setDownloadedMessage();
		}
		return view;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(KEY_CAT_FILE, catFile);
	}

	private void setStatusMessage(int res) {
		String status = getResources().getString(res);
		String labelText = getResources().getString(R.string.home_status_label_text);
		statusLabel.setText(labelText + " " + status);
	}

	private void setIdleMessage() {
		setStatusMessage(R.string.home_status_idle);
	}

	private void setDownloadingMessage() {
		setStatusMessage(R.string.home_status_downloading);
	}
	private void setDownloadedMessage() {
		setStatusMessage(R.string.home_status_downloaded);
	}


	private static class FragmentProgressHandler extends Handler implements ImageDownloader.ProgressListener {
		protected final int MESSAGE_PROGRESS = 0;


		WeakReference<HomeFragment> fragmentRef;

		public FragmentProgressHandler(HomeFragment fragment) {
			this.fragmentRef = new WeakReference<>(fragment);
		}

		public void updateProgress(int progress) {
			obtainMessage(MESSAGE_PROGRESS, progress).sendToTarget();
		}

		@Override
		public void handleMessage(Message msg) {
			if (msg.what != MESSAGE_PROGRESS) {
				return;
			}
			HomeFragment fragment = fragmentRef.get();
			if (fragment == null) {
				return;
			}

			int progress = (Integer) msg.obj;
			fragment.downloadProgressBar.setProgress(progress);
		}
	}

	private class ImageDownloaderCallbacks implements LoaderManager.LoaderCallbacks<String> {
		@Override
		public Loader<String> onCreateLoader(int id, Bundle args) {
			Context context = getContext();
			String url = context.getResources().getString(R.string.cat_url);
			catFile = null;
			return new ImageDownloader(context, url, handler);
		}

		@Override
		public void onLoadFinished(Loader<String> loader, String data) {
			if (data == null) {
				Toast.makeText(getContext(), R.string.download_failed_message, Toast.LENGTH_SHORT).show();
				button.setEnabled(true);
				setIdleMessage();
			} else {
				setDownloadedMessage();
				catFile = data;
				button.setText(R.string.home_button_text_open);
				button.setEnabled(true);
				button.setOnClickListener(new OpenClickListener());

				File imagePath = ImageUtils.getImagesPath();
				File imageFile = new File(imagePath, catFile);
				Uri uri = ImagesProvider.getUriForFileName("com.moncruist.demotask.imagesprovider", imageFile.getName());
				try {
					MediaStore.Images.Media.insertImage(getContext().getContentResolver(), imageFile.getAbsolutePath(), imageFile.getName(), "");
				} catch (FileNotFoundException e) {
					Log.e(TAG, e.getMessage());
				}
			}
		}

		@Override
		public void onLoaderReset(Loader<String> loader) {

		}
	}

	private class DownloadClickListener implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			int permissionCheck = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.INTERNET);
			if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
				Log.e(TAG, "Internet permission denied");
				Toast.makeText(getContext(), R.string.no_internet_permission_message, Toast.LENGTH_SHORT).show();
				return;
			}

			permissionCheck = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
			if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
				Log.e(TAG, "Write external storage permission denied");
				Toast.makeText(getContext(), R.string.no_write_ext_storage_permission_message, Toast.LENGTH_SHORT).show();
				return;
			}

			LoaderManager lm = getLoaderManager();
			if (lm.getLoader(LOADER_ID_CAT) == null) {
				lm.initLoader(LOADER_ID_CAT, null, loaderCallbacks);
			} else {
				lm.restartLoader(LOADER_ID_CAT, null, loaderCallbacks);
			}
			setDownloadingMessage();
			downloadProgressBar.setVisibility(View.VISIBLE);
			button.setEnabled(false);
		}
	}

	private class OpenClickListener implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent();
			intent.setAction(Intent.ACTION_VIEW);
			File imagePath = new File(getContext().getFilesDir(), "images");
			File imageFile = new File(imagePath, catFile);
			Uri uri = ImagesProvider.getUriForFileName("com.moncruist.demotask.imagesprovider", imageFile.getName());
			intent.setType("image/*");
			List<ResolveInfo> resInfoList = getContext().getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
			if (resInfoList.size() > 0) {
				intent.setData(uri);
				startActivity(intent);
			} else {
				Log.w(TAG, "No activities to open image");
				Toast.makeText(getContext(), R.string.no_apps_for_open_image_message, Toast.LENGTH_SHORT).show();
			}
		}
	}
}
