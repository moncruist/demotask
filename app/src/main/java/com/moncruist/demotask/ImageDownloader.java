package com.moncruist.demotask;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class ImageDownloader extends AsyncTaskLoader<String> {

	public interface ProgressListener {
		void updateProgress(int progress);
	}

	private static final String TAG = "ImageDownloader";
	private static final int DOWNLOAD_BUFFER_SIZE = 4096;

	private String urlString;
	private String imageFileName;
	private ProgressListener listener;

	public ImageDownloader(Context context, String url, @Nullable ProgressListener progressListener) {
		super(context);
		this.urlString = url;
		this.listener = progressListener;
	}

	@Override
	protected void onStartLoading() {
		if (imageFileName != null) {
			// If we currently have a result available, deliver it
			// immediately.
			deliverResult(imageFileName);
		}
		if (takeContentChanged() || imageFileName == null) {
			// If the data has changed or is not currently available, start to load
			forceLoad();
		}
	}

	@Override
	protected void onStopLoading() {
		cancelLoad();
	}

	@Override
	protected void onReset() {
		super.onReset();
		onStopLoading();
		imageFileName = null;
	}

	@Override
	public void deliverResult(String path) {
		imageFileName = path;
		if (isStarted()) {
			super.deliverResult(path);
		}
	}

	@Override
	public String loadInBackground() {
		InputStream input = null;
		OutputStream output = null;
		OkHttpClient client = new OkHttpClient();
		String fileName = null;

		try {
			URL url = new URL(urlString);
			Call call = client.newCall(new Request.Builder().get().url(url).build());
			Response response = call.execute();

			if (response.code() != 200) {
				return null;
			}

			fileName = urlString.substring(urlString.lastIndexOf('/')+1, urlString.length());
			long fileLength = response.body().contentLength();

			input = response.body().byteStream();
			File folderPath = ImageUtils.getImagesPath();
			folderPath.mkdirs();
			output = new FileOutputStream(new File(folderPath, fileName));

			byte data[] = new byte[DOWNLOAD_BUFFER_SIZE];
			long total = 0;
			int readed;

			while((readed = input.read(data)) != -1) {
				if (isLoadInBackgroundCanceled()) {
					input.close();
					output.close();
					return null;
				}

				total += readed;

				// send progress
				if (fileLength > 0 && listener != null) {
					listener.updateProgress((int) (total * 100 / fileLength));
				}
				output.write(data, 0, readed);
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			return null;
		} finally {
			try {
				if (output != null) {
					output.close();
				}
				if (input != null) {
					input.close();
				}
			} catch (IOException e) {
				Log.e(TAG, e.getMessage());
			}
		}
		return fileName;
	}
}
