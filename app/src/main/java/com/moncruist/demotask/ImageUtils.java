package com.moncruist.demotask;

import android.content.Context;
import android.os.Environment;

import java.io.File;

public class ImageUtils {
	public static File getImagesPath() {
		return new File(Environment.getExternalStorageDirectory().toString() + File.separator + "demotask" + File.separator + "images");
	}
}
