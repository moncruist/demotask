package com.moncruist.demotask;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.OpenableColumns;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;

public class ImagesProvider extends ContentProvider {
	private final static String[] OPENABLE_PROJECTIONS = { OpenableColumns.DISPLAY_NAME, OpenableColumns.SIZE };
	private final static String imagesFolder = "images";
	private static final String TAG = "ImagesProvider";

	public ImagesProvider() {
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		File file = getFileForUri(uri);
		return file.delete() ? 1 : 0;
	}

	@Override
	public String getType(Uri uri) {
		Log.d(TAG, "getType");
		return "image/jpeg";
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		throw new UnsupportedOperationException("No external inserts");
	}

	@Override
	public boolean onCreate() {
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
						String[] selectionArgs, String sortOrder) {
		Log.d(TAG, "query: " + uri.toString());
		if (projection == null) {
			projection = OPENABLE_PROJECTIONS;
		}

		File file = getFileForUri(uri);

		String[] cols = new String[projection.length];
		Object[] values = new Object[projection.length];
		int idx = 0;
		for (String col : projection) {
			if (OpenableColumns.DISPLAY_NAME.equals(col)) {
				cols[idx] = OpenableColumns.DISPLAY_NAME;
				values[idx++] = file.getName();
			} else if (OpenableColumns.SIZE.equals(col)) {
				cols[idx] = OpenableColumns.SIZE;
				values[idx++] = file.length();
			}
		}

		cols = copyOf(cols, idx);
		values = copyOf(values, idx);

		MatrixCursor cursor = new MatrixCursor(cols, 1);
		cursor.addRow(values);

		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
					  String[] selectionArgs) {
		throw new UnsupportedOperationException("No external updates");
	}

	@Nullable
	@Override
	public ParcelFileDescriptor openFile(Uri uri, String mode) throws FileNotFoundException {
		File file = getFileForUri(uri);
		return ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);
	}

	public static Uri getUriForFileName(String authority, String fileName) {
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("content")
				.authority(authority)
				.appendPath(fileName);
		return builder.build();
	}

	private File getFileForUri(Uri uri) {
		String fileLocation = ImageUtils.getImagesPath().toString() + File.separator + uri.getPath();
		return new File(fileLocation);
	}

	private static String[] copyOf(String[] original, int newLength) {
		final String[] result = new String[newLength];
		System.arraycopy(original, 0, result, 0, newLength);
		return result;
	}

	private static Object[] copyOf(Object[] original, int newLength) {
		final Object[] result = new Object[newLength];
		System.arraycopy(original, 0, result, 0, newLength);
		return result;
	}
}
